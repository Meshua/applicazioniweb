#ifndef __DOOR_LOCK__
#define __DOOR_LOCK__

class DoorLock {
  public:
    DoorLock(int pin);
    void openLock();
    void closeLock();
    bool isLock();
  private:
    int pin;
};

#endif
