#include "Arduino.h"
#include "MessageManager.h"

String content;
MessageManagerClass MessageManager;

bool MessageManagerClass::isMsgAvailable() {
  return msgAvailable;
}

Msg* MessageManagerClass::receiveMsg() {
  Serial.println("receiveMSG");
  if (msgAvailable) {
    Msg* msg = currentMsg;
    Serial.println(msg -> getContent());
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;
  } else {
    return NULL;
  }
}

void MessageManagerClass::init() {
  Serial.begin(9600);
  content = "";
  currentMsg = NULL;
  msgAvailable = false;
}

void MessageManagerClass::sendMsg(String msg) {
  Serial.println(msg);
}

void serialEvent() {
  delay(50);

  while (Serial.available()) {
    content += (char)Serial.read();
  }

  MessageManager.currentMsg = new Msg(content);
  MessageManager.msgAvailable = true;
}
