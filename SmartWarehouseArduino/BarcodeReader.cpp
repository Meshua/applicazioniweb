#include "BarcodeReader.h"

#include <SoftwareSerial.h>
SoftwareSerial ss(BR_RX, BR_TX);

BarcodeReader::BarcodeReader() {
  ss.begin(9600);
  code = "";
}

String BarcodeReader::getCode() {
  return code;
}


bool BarcodeReader::isAvailable() {
  if (ss.available()) {
    code = ss.read();
    return true;
  }
  return false;
}
