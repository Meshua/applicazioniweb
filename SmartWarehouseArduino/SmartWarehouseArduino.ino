#include "Scheduler.h"
#include "config.h"
#include "TemperatureTask.h"
#include "GoodsTask.h"
#include "Arduino.h"
#include "BottleDistance.h"
#include "SharedContext.h"
#include "DoorTask.h"

Scheduler scheduler;

void setup() {
  Serial.begin(9600);
  scheduler.init(50);

  SharedContext* context = new SharedContext();

  BottleDistance* u1 = new BottleDistance(U1_ECHO, U1_TRIG);
  BottleDistance* u2 = new BottleDistance(U2_ECHO, U2_TRIG);
  Temperature* tmp = new Temperature(TMP_PIN);
  DoorLock* lock = new DoorLock(LOCK_PIN);
  BarcodeReader* reader = new BarcodeReader();

  Task* t0 = new TemperatureTask(tmp);
  t0 -> init(250);
  scheduler.addTask(t0);

  Task* t1 = new GoodsTask(u1, u2, lock, reader, context, "8012345678903");
  t1 -> init(50);
  scheduler.addTask(t1);

  Task* t2 = new DoorTask(lock, context);
  t2 -> init(150);
  scheduler.addTask(t2);

  Serial.println("End setup");
}

void loop() {
  scheduler.schedule();
}
