#include "DoorLock.h"
#include "Arduino.h"

DoorLock::DoorLock(int pin){
  this->pin = pin;
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
}

void DoorLock::openLock(){
  digitalWrite(pin, HIGH);
}

void DoorLock::closeLock(){
  digitalWrite(pin, LOW);
}

bool DoorLock::isLock(){
  return digitalRead(pin)==LOW;
}
