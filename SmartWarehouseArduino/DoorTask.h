#ifndef __DOOR_TASK__
#define __DOOR_TASK__

#include "Task.h"
#include "MessageManager.h"
#include "SharedContext.h"
#include "DoorLock.h"

class DoorTask: public Task {
  public:
    DoorTask(DoorLock* lock, SharedContext* context);
    void init(int period);
    void tick();

  private:
    enum {
      PORTA_APERTA,
      PORTA_CHIUSA
    } state;
    DoorLock* lock;
    SharedContext* context;
};

#endif
