#include "DoorTask.h"
#include "Arduino.h"

DoorTask::DoorTask(DoorLock* lock, SharedContext* context) {
  this->lock = lock;
  this->context = context;
}

void DoorTask::init(int period) {
  Task::init(period);
  state = PORTA_CHIUSA;
}

void DoorTask::tick() {
  switch (state) {
    case PORTA_CHIUSA: {
        if (MessageManager.isMsgAvailable()) {
          Msg* msg = MessageManager.receiveMsg();
          if (msg->getContent() == "LOGIN") {
            context->setOpenDoor(true);
            lock->openLock();
            state = PORTA_APERTA;
          }
        }
        break;
      }
    case PORTA_APERTA: {
        if (lock->isLock()) {
          context->setOpenDoor(false);
          state = PORTA_CHIUSA;
        }
      }
  }
}
