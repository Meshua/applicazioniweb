#include "Temperature.h"
#include "Arduino.h"

Temperature::Temperature(int analogPin){
  this->pin = analogPin;
}

float Temperature::getTemperature(){
  int val_read = analogRead(pin);

  return (((val_read /1024.0) * 5.0) - .5) * 100;
}
