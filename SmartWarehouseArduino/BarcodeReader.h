#ifndef __BARCODE__
#define __BARCODE__
#include "Arduino.h"
#include "Config.h"
class BarcodeReader {
  public:
    BarcodeReader();
    String getCode();
    bool isAvailable();
    
  private:
    String code;
};

#endif
