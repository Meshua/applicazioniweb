#include "TemperatureTask.h"
#include "Arduino.h"

TemperatureTask::TemperatureTask(Temperature* tmp){
  this->tmp = tmp;
}

void TemperatureTask::init(int period){
  Task::init(period);
  state = ATTESA_TIMEOUT;
  tTemp = 0;
}

void TemperatureTask::tick(){
  switch(state){
    case ATTESA_TIMEOUT: {
      tTemp++;
      if (tTemp > TIMEOUT_TEMP) state = COMUNICAZIONE_TEMPERATURA;
      
      break;
    }
    case COMUNICAZIONE_TEMPERATURA: {
      String temp = (String) tmp->getTemperature();
    
      MessageManager.sendMsg("Temp:" + temp);
      tTemp = 0;
      state = ATTESA_TIMEOUT;
      break;
    }
  }
}
