#ifndef __GOODS_TASK__
#define __GOODS_TASK__

#include "Task.h"
#include "config.h"
#include "SharedContext.h"
#include "BottleDistance.h"
#include "MessageManager.h"
#include "DoorLock.h"
#include "BarcodeReader.h"
#include "SharedContext.h"

class GoodsTask: public Task { 
  public: 
    GoodsTask(BottleDistance* u1, BottleDistance* u2, DoorLock* lock, BarcodeReader* reader, SharedContext* context, String productType);
    void init(int period);  
    void tick();
    
  private:
  int t;
  String productType;
  String productTypeDefault;
  BottleDistance* u1;
  BottleDistance* u2;
  DoorLock* lock;
  BarcodeReader* reader;
  int samplingT;
  SharedContext* context;
  
  enum {  PORTA_CHIUSA,
          PORTA_APERTA,
          MERCE_NON_RILEVATA,
          MERCE_IN_ENTRATA_DEFAULT,
          MERCE_DEPOSITATA,
          SCAN_PRODOTTO,
          MERCE_IN_ENTRATA_CON_SCAN,
          MERCE_IN_USCITA,
          MERCE_PRELEVATA_DEFAULT,
          MERCE_PRELEVATA_CON_SCAN,
          MERCE_PRELEVATA} state;
};

#endif
