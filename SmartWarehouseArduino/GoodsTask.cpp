#include "GoodsTask.h"
#include "Arduino.h"

GoodsTask::GoodsTask(BottleDistance* u1, BottleDistance* u2, DoorLock* lock,  BarcodeReader* reader, SharedContext* context, String productType) {
  this->u1 = u1;
  this->u2 = u2;
  this->lock = lock;
  this->reader = reader;
  this->productTypeDefault = productType;
  this->context = context;
}

void GoodsTask::init(int period) {
  Task::init(period);
  state = PORTA_CHIUSA;
  t = 0;
  this->productType = "";
  samplingT = SAMPLING_TIME_DEFAULT;
}

void GoodsTask::tick() {
  switch (state) {
    case PORTA_CHIUSA: {
        if (context->isOpenDoor()) {
          state = MERCE_NON_RILEVATA;
        }
        break;
      }

    case MERCE_NON_RILEVATA: {
        t = 0;
        productType = "";
        
        int u1Distance = u1->getDistance();
        int u2Distance = u2->getDistance();

        samplingT += 1;

        if (u1Distance < DETECTION_DIST && productType == "" && samplingT > SAMPLING_TIME) {
          state = MERCE_IN_ENTRATA_DEFAULT;
        } else if (reader->isAvailable()) {
          state = SCAN_PRODOTTO;
        } else if (u2Distance < DETECTION_DIST && samplingT > SAMPLING_TIME) {
          state = MERCE_IN_USCITA;
        } else if (!context->isOpenDoor()) {
          state = PORTA_CHIUSA;
        }
        break;
      }

    case MERCE_IN_ENTRATA_DEFAULT: {
        productType = productTypeDefault;

        if (u2->getDistance() < DETECTION_DIST) state = MERCE_DEPOSITATA;

        break;
      }

    case MERCE_DEPOSITATA: {
        MessageManager.sendMsg("+:" + productType);
        state = MERCE_NON_RILEVATA;

        samplingT = 0;
        break;
      }

    case SCAN_PRODOTTO: {
        productType = reader->getCode();
        
        if (u1->getDistance() < DETECTION_DIST) state = MERCE_IN_ENTRATA_CON_SCAN;
        break;
      }

    case MERCE_IN_ENTRATA_CON_SCAN: {
        if (u2->getDistance() < DETECTION_DIST) state = MERCE_DEPOSITATA;

        break;
      }

    case MERCE_IN_USCITA: {
        if (u1->getDistance() < DETECTION_DIST) state = MERCE_PRELEVATA_DEFAULT;

        break;
      }

    case MERCE_PRELEVATA_DEFAULT: {
        productType = productTypeDefault;
        t++;

        if (t > TEMP_SCAN) {
          state = MERCE_PRELEVATA;
        } else if (reader->isAvailable()) {
          state = MERCE_PRELEVATA_CON_SCAN;
        }

        break;
      }

    case MERCE_PRELEVATA_CON_SCAN: {
        productType = reader->getCode();

        state = MERCE_PRELEVATA;

        break;
      }

    case MERCE_PRELEVATA: {
        MessageManager.sendMsg("-:" + productType);

        state = MERCE_NON_RILEVATA;

        samplingT = 0;
        break;
      }
  }
}
