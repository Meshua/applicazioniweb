#include "SharedContext.h"

SharedContext::SharedContext() {
  openDoor = false;
}

//Door opening / closing
void SharedContext::setOpenDoor(bool door) {
  this->openDoor = door;
}

bool SharedContext::isOpenDoor() {
  return openDoor;
}
