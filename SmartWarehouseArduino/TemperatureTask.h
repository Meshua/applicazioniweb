#ifndef __TEMPERATURE_TASK__
#define __TEMPERATURE_TASK__

#include "Task.h"
#include "config.h"
#include "Temperature.h"
#include "MessageManager.h"

class TemperatureTask: public Task { 
  public: 
    TemperatureTask(Temperature* tmp);
    void init(int period);  
    void tick();
    
  private:  
    enum { 
          ATTESA_TIMEOUT, 
          COMUNICAZIONE_TEMPERATURA
         } state;
    int tTemp;
    Temperature* tmp;
};

#endif
