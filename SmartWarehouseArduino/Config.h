#ifndef __CONFIG__
#define __CONFIG__

//DIGITAL PIN
#define U1_ECHO 9
#define U1_TRIG 8
#define U2_ECHO 11
#define U2_TRIG 10
#define LOCK_PIN 7
#define BR_RX 0
#define BR_TX 1

//ANALOG PIN
#define TMP_PIN A0

//CONSTANT
//timeout for temperature communication
#define TIMEOUT_TEMP 15
//maximum time that the user has to scan the goods after having picked them up
//3sec circa
#define TEMP_SCAN 60

#define DETECTION_DIST 5

#define SAMPLING_TIME 3
#define SAMPLING_TIME_DEFAULT 5

#endif
