#ifndef __BOTTLE_DISTANCE__
#define __BOTTLE_DISTANCE__

class BottleDistance {
  public:
    BottleDistance(int echo, int trig);
    int getDistance();

  private:
    int echo;
    int trig;
    int distance;
    long duration;
};

#endif
