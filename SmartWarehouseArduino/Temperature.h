#ifndef __TEMPERATURE__
#define __TEMPERATURE__

class Temperature {
  public:
    Temperature(int analogPin);
    float getTemperature();

  private:
    int pin;
};

#endif
