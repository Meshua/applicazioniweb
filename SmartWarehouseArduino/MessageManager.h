#ifndef __MSG_MANAGER__
#define __MSG_MANAGER__

#include "Arduino.h"

class Msg {
  String content;

  public:
  Msg(String content){
    this->content = content;
  }

  String getContent(){
    return content;
  }
};

class MessageManagerClass { 
  public: 
    Msg* currentMsg;
    bool msgAvailable;

    void init();
    bool isMsgAvailable();
    Msg* receiveMsg();
    void sendMsg(String msg);
};

extern MessageManagerClass MessageManager;

#endif
