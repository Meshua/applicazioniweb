#ifndef __SHAREDCONTEXT__
#define __SHAREDCONTEXT__
#include "Arduino.h"

class SharedContext {
  public:
    SharedContext();
    void setOpenDoor(bool door);
    bool isOpenDoor();

  private:
    bool openDoor;

};

#endif
