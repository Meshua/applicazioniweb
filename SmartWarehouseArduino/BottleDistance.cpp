#include "BottleDistance.h"
#include "Arduino.h"

BottleDistance::BottleDistance(int echo, int trig){
  this->echo = echo;
  this->trig = trig;

  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
}

int BottleDistance::getDistance(){
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  duration = pulseIn(echo, HIGH);
  
  distance = duration*0.034/2;
  return distance;
}
