module.exports = function(app) {
	var warehouseController = require('../controllers/warehouseController');
	app.route('/').get(warehouseController.show_index);
	app.route('/api/loginSubmit').post(warehouseController.auth);
	app.route('/api/login_face_recognition').post(warehouseController.login_face_recognition);
	app.route('/api/list_goods').post(warehouseController.list_goods);
	app.route('/api/list_staff').get(warehouseController.list_staff);
	app.route('/api/insert_stock_good').post(warehouseController.insert_stock_good);
	app.route('/api/insert_stock_barcode').get(warehouseController.insert_stock_barcode);
	app.route('/api/remove_stock_good').post(warehouseController.remove_stock_good);
	app.route('/api/remove_stock_barcode').get(warehouseController.remove_stock_barcode);
	app.route('/api/delete_good').post(warehouseController.delete_good);
	app.route('/api/list_supplier').get(warehouseController.list_supplier);
	app.route('/api/temperature').get(warehouseController.temperature);
	app.route('/api/get_temperature').get(warehouseController.get_temperature);
};
