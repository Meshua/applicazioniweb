import serial


ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)


def serial_init():
    ser.flush()
    return ser


def serial_read():
    if ser.in_waiting > 0:
        return ser.readline().decode('latin-1').rstrip()
    return ""
    

def serial_write(msg):
    ser.write(msg.encode('latin-1'))
