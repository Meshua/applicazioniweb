import requests
import SerialCommunication
import FaceRecognition

base_url = "http://192.168.1.57:8080/api"

def main():
    state = "PORTA_CHIUSA"
    SerialCommunication.serial_init()

    while True:
        if state == "PORTA_APERTA":
            line = SerialCommunication.serial_read()
            if line != "":                      
                line = line.split(":")
                read_temperature(line)
                if line[0] == "+":
                    url = base_url + 'h/insert_stock_barcode'
                    requests.get(url + '?barcode=' + line[1])
                if line[0] == "-":
                    url = base_url + '/remove_stock_barcode'
                    requests.get(url + '?barcode=' + line[1])
        elif state == "PORTA_CHIUSA":
            camera = FaceRecognition.start_recognition()
            face_id = FaceRecognition.recognition(camera)
            while face_id == 0:
                face_id = FaceRecognition.recognition(camera)
                read_temperature(SerialCommunication.serial_read().split(":"))

            SerialCommunication.serial_write("LOGIN")
            url = base_url + '/login_face_recognition'
            my_obj = {'staff_id' : face_id}
            x = requests.post(url, data = my_obj)

            FaceRecognition.stop_recognition(camera)
            state = "PORTA_APERTA"


def read_temperature(line):
    if line[0] == "Temp":
        url = base_url + '/temperature'
        x = requests.get(url + '?temp=' + line[1])

if __name__ == "__main__":
    main()
