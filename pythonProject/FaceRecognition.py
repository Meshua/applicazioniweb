import cv2

recognizer = cv2.face.createLBPHFaceRecognizer()
recognizer.load('trainer/trainer.yml')

frontal_face_detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")


def start_recognition():
    return cv2.VideoCapture(0)


def stop_recognition(camera):
    camera.release()
    cv2.destroyAllWindows()


def recognition(camera):

    _, frame = camera.read()
    frame_grayscale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = frontal_face_detector.detectMultiScale(frame_grayscale, 1.2, 4)

    for (x, y, w, h) in faces:
        # disegna un rettangolo attorno alla fiaccia
        cv2.rectangle(frame, (x-20, y-20), (x + w + 20, y + h + 20), (255, 255, 255), 1)
        return int(recognizer.predict(frame_grayscale[y:y+h, x:x+w])[0])
    return 0
